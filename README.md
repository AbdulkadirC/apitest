# Api Testing Assignment

The project was automated inside the GitLab CI. <br>
The code is inside the Gitlab repository, and can be clone to local computer.<br>
Once push the updated code to the remote repository, the pipeline will be triggered automatically, and
the Test Report (**surefire-report.html**) is accessible as **job artifacts (Gitlab>CI/CD>Jobs)**<br>
Also junit test results can be available in **Gitlab>CI/CD>Pipelines>Pipeline>Tests**<br>
By clicking this test button, if there is any failed test scenario, the System output (expected and actual result) for the failed text scenarios is visible.

(https://gitlab.com/AbdulkadirC/apitest)

## How to install and run
- Clone the repository from Gitlab
- Install Docker
- cd into the project directory
- First, create a 'target' directory for reports to be placed in after the tests are done in docker file

```
mkdir target
``` 
- Build the docker image from the Dockerfile, you should be in the same directory with the Dockerfile for this command to work as expected:
```
docker build -t bynder-api-test-img .
```   

- After build is completed, run the image by binding the target directory to the image:
```
docker run -it -v ${PWD}/target:/app/target bynder-api-test-img
``` 

After the tests are done, the container will exit and hand over the CLI back. <br>
Then check the **'target/site/surefire-reports.html'** directory for the  surefire html report.
<br>



You can see the endpoints interactions below;

<br>


**Test Scenarios**

**Scenario:**  Call top rated GET endpoint with Valid Api_key<br>
&nbsp; **Given** I accept ContentType.JSON<br>
&nbsp; **When** I call top_rated GET endpoint with valid api_key param<br>
&nbsp; **Then** the statusCode should be 200<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **And** the default page in the body should be 1<br>
&nbsp; **And** the results, total_pages and the total_results should not be null<br><br>

**Scenario:**  Call top_rated GET endpoint with Valid Api_key and Page param<br>
&nbsp; **Given** I accept ContentType.JSON<br>
&nbsp; **When** I call top_rated GET endpoint with valid api_key param and page param 3<br>
&nbsp; **Then** the statusCode should be 200<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **And** the default page in the body should be 3<br>
&nbsp; **And** the results, total_pages and the total_results should not be null <br><br>

**Scenario:**  Call top_rated GET endpoint with Invalid Api_key<br>
&nbsp; **Given** I accept ContentType.JSON<br>
&nbsp; **When** I call top_rated GET endpoint with Invalid api_key param<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **And** the status_code in the response body should be 7<br>
&nbsp; **And** the status_message in the body should be "Invalid API key: You must be granted a valid key."<br><br>


**Scenario:**  Call top_rated GET endpoint with Valid Auth<br>
&nbsp; **Given** I accept ContentType.JSON<br>
&nbsp; **When** I call top_rated GET endpoint with Valid Auth<br>
&nbsp; **Then** the statusCode should be 200<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **And** the default page in the body should be 1<br>
&nbsp; **And** the results, total_pages and the total_results should not be null<br><br>


**Scenario:**  Call top_rated GET endpoint with Invalid Auth<br>
&nbsp; **Given** I accept ContentType.JSON<br>
&nbsp; **When** I call top_rated GET endpoint with Invalid Auth<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **And** the status_code in the response body should be 7<br>
&nbsp; **And** the status_message in the body should be  "Invalid API key: You must be granted a valid key."<br><br>


**Scenario:**  Call movie rating POST endpoint with Invalid SessionID<br>
&nbsp; **Given** I set the Valid api_key and Invalid Session_Id querryParam with a valid body<br>
&nbsp; **When** I call the rating POST endpoint<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **And** the status_code in the response body should be 3<br>
&nbsp; **And** the status_message in the response body should be "Authentication failed: You do not have permissions to access the service."<br><br>


**Scenario:**  Call movie rating POST endpoint with Invalid Api_key<br>
&nbsp; **Given** I set the Invalid api_key and Valid Session_Id querryParam with a valid body<br>
&nbsp; **When** I call the rating POST endpoint<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **And** the status_code in the response body should be 7<br>
&nbsp; **And** the status_message in the response body should be "Invalid API key: You must be granted a valid key."<br><br>

**Scenario:**  Call movie rating POST endpoint without Api_key<br>
&nbsp; **Given** I set Valid Session_Id querryParam but without Api_key with a valid body<br>
&nbsp; **When** I call the rating POST endpoint<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **And** the status_code in the response body should be 7<br>
&nbsp; **And** the status_message in the response body should be "Invalid API key: You must be granted a valid key."<br><br>


**Scenario:**  Call movie rating POST endpoint without Session_Id<br>
&nbsp; **Given** I set Valid Api_key querryParam but without Session_Id with a valid body<br>
&nbsp; **When** I call the rating POST endpoint<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **And** the status_code in the response body should be 3<br>
&nbsp; **And** the status_message in the response body should be "Authentication failed: You do not have permissions to access the service."<br><br>

