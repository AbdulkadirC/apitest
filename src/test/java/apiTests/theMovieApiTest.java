package apiTests;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import utilities.ConfigurationReader;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.equalTo;

public class theMovieApiTest {

    @BeforeAll
    public static void setUp(){
        RestAssured.baseURI= ConfigurationReader.get("baseURI");
        RestAssured.basePath=ConfigurationReader.get("basePath");
    }

    @DisplayName("Call top_rated GET endpoint with Valid Api_key")
    @Test
    public void TopRatedGETendpointWithValidApiKey(){
        given()
                .accept(ContentType.JSON).
                when()
                .param("api_key", ConfigurationReader.get("api_key")).get("/top_rated").
                then()
                .statusCode(200).contentType(ContentType.JSON).assertThat()
                .body("page", equalTo(1))
                .body("results", notNullValue())
                .body("total_pages", notNullValue())
                .body("results.backdrop_path[0]", containsString(".jpg"))
                .body("total_results", notNullValue());

    }

    @DisplayName("Call top_rated GET endpoint with Valid Api_key and Page param")
    @Test
    public void TopRatedGETendpointWithValidApiKeyAndPageParam(){
        given()
                .accept(ContentType.JSON).
                when()
                .param("api_key", ConfigurationReader.get("api_key"))
                .param("page", 3).get("/top_rated").
                then()
                .statusCode(200).contentType(ContentType.JSON).assertThat()
                .body("page", equalTo(3))
                .body("results", notNullValue())
                .body("total_pages", notNullValue())
                .body("results.backdrop_path[0]", containsString(".jpg"))
                .body("total_results", notNullValue());
    }

    @DisplayName("Call top_rated GET endpoint with Invalid Api_key")
    @Test
    public void TopRatedGETendpointWithInvalidApiKey(){
        given()
                .accept(ContentType.JSON).
                when()
                .param("api_key", "invalidApiKey").get("/top_rated").
                then()
                .statusCode(401).contentType(ContentType.JSON).assertThat()
                .body("status_code", equalTo(7))
                .body("status_message", equalTo("Invalid API key: You must be granted a valid key."))
                .body("success", equalTo(false));
    }

    @DisplayName("Call top_rated GET endpoint with Valid Auth")
    @Test
    public void TopRatedGETendpointWithValidAuth(){
        given()
                .accept(ContentType.JSON).
                when()
                .auth().oauth2(ConfigurationReader.get("token")).get("/top_rated").
                then()
                .statusCode(200).contentType(ContentType.JSON).assertThat()
                .body("page", equalTo(1))
                .body("results", notNullValue())
                .body("total_pages", notNullValue())
                .body("results.backdrop_path[0]", containsString(".jpg"))
                .body("total_results", notNullValue());
    }

    @DisplayName("Call top_rated GET endpoint with Invalid Auth")
    @Test
    public void TopRatedGETendpointWithInvalidAuth(){
        given()
                .accept(ContentType.JSON).
                when()
                .auth().oauth2("invalidAuth").get("/top_rated").
                then()
                .statusCode(401).contentType(ContentType.JSON).
                assertThat()
                .body("status_code", equalTo(7))
                .body("status_message", equalTo("Invalid API key: You must be granted a valid key."))
                .body("success", equalTo(false))
        ;
    }

    @DisplayName("Call movie rating POST endpoint with Invalid SessionID")
    @Test
    public void CallMovieRatingPOSTendpointWithInvalidSessionID(){

        Map<String,Object> movieMap = new HashMap<>();
        movieMap.put("value" , 10);

        given()
                .contentType(ContentType.JSON)
                .queryParam("api_key", ConfigurationReader.get("api_key"))
                .queryParam("session_id", "invalidSessionId")
                .body(movieMap)
                .accept(ContentType.JSON).
                when()
                .post("/225/rating").
                then()
                .statusCode(401)
                .contentType(ContentType.JSON).
                assertThat()
                .body("success", equalTo(false))
                .body("status_code", equalTo(3))
                .body("status_message", equalTo("Authentication failed: You do not have permissions to access the service."))
        ;
    }

    @DisplayName("Call movie rating POST endpoint with Invalid Api_key")
    @Test
    public void MovieRatingPOSTWithInvalidApiKey(){

        Map<String,Object> movieMap = new HashMap<>();
        movieMap.put("value" , 10);

        given()
                .contentType(ContentType.JSON)
                .queryParam("api_key", "invalidApiKey")
                .queryParam("session_id", ConfigurationReader.get("session_id"))
                .body(movieMap)
                .accept(ContentType.JSON).
                when()
                .post("/225/rating").
                then()
                .statusCode(401).contentType(ContentType.JSON).assertThat()
                .body("status_code", equalTo(7))
                .body("status_message", equalTo("Invalid API key: You must be granted a valid key."))
                .body("success", equalTo(false));
    }

    @DisplayName("Call movie rating POST endpoint without Api_key")
    @Test
    public void MovieRatingPOSTendpointWithoutApiKey(){

        Map<String,Object> movieMap = new HashMap<>();
        movieMap.put("value" , 10);

        given()
                .contentType(ContentType.JSON)
                .queryParam("session_id", ConfigurationReader.get("session_id"))
                .body(movieMap).accept(ContentType.JSON).
                when()
                .post("/225/rating").
                then()
                .statusCode(401).contentType(ContentType.JSON).assertThat()
                .body("status_code", equalTo(7))
                .body("status_message", equalTo("Invalid API key: You must be granted a valid key."))
                .body("success", equalTo(false));
    }

    @DisplayName("Call movie rating POST endpoint without Session_Id")
    @Test
    public void MovieRatingPOSTendpointWithoutSessionId(){

        Map<String,Object> movieMap = new HashMap<>();
        movieMap.put("value" , 10);

        given()
                .contentType(ContentType.JSON)
                .queryParam("api_key", ConfigurationReader.get("api_key"))
                .body(movieMap).accept(ContentType.JSON).
                when()
                .post("/225/rating").
                then()
                .statusCode(401).contentType(ContentType.JSON).assertThat()
                .body("status_code", equalTo(3))
                .body("status_message", equalTo("Authentication failed: You do not have permissions to access the service."))
                .body("success", equalTo(false));
    }

}
